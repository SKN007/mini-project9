# Mini projrct 9

Streamlit App with a Hugging Face Model

## Requiements

Create a website using Streamlit

Connect to an open source LLM (Hugging Face)

Deploy model via Streamlit or other service (accessible via browser)

## Steps

1. Create a python file and implement the streamlit code. My streamlit app's function is to translate English to Chinese.
   ```
   import streamlit as st
   from transformers import pipeline

   st.title('English to Chinese')
   text = st.text_area("Input your text in English:", value='', height=150, max_chars=None, key=None)

   translator = pipeline("translation_en_to_zh", model="Helsinki-NLP/opus-mt-en-zh")

   if st.button('Translate'):
       if text:
           translation = translator(text, max_length=40)
           st.write(translation[0]['translation_text'])
       else:
           st.write('Text...')

   ```
2. Create a file "requirements.txt" and add all requirements to it.
3. Sign up in streamlit's official website and connect to github's account.
4. Create a repository in Github and push all the files.
5. Deploy the app in streamlit.

### Result display

[My app.](https://mini-project9-uwjefnjzdm22nxekgfe2zu.streamlit.app)

![img](sc1.png)
